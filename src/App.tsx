import Footer from './components/Layout/Footer/Footer'
import Header from './components/Layout/Header/Header'
import Main from './components/Layout/Main/Main'
import Menu from './components/Layout/Menu/Menu'
import Right from './components/Layout/Right/Right'

function App() {
 return (
  <div className='App Grid-container'>
   <Menu />
   <Header />
   <Main />
   <Right />
   <Footer />
  </div>
 )
}

export default App
