import SidebarMenu from '../SidebarMenu/SidebarMenu'
import './Sidebar.scss'

export default function Sidebar() {
 return (
  <div className='Sidebar'>
   <SidebarMenu />
  </div>
 )
}
