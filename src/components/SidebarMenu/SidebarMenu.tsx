import { menu } from '../../constants/constantes'
import './SidebarMenu.scss'

export default function SidebarMenu() {
 return (
  <div className='SidebarMenu'>
   <ul>
    {menu.map(({ id, name, icon }) => (
     <li key={id}>
      <div className='SidebarMenu-item'>
       <img src={icon} />
       <a href='#'>{name}</a>
      </div>
     </li>
    ))}
   </ul>
  </div>
 )
}
