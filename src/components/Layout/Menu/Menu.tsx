import Sidebar from '../../Sidebar/Sidebar'

export default function Menu() {
 return (
  <div className='Menu'>
   <Sidebar />
  </div>
 )
}
