import Navbar from '../../Navbar/Navbar'
import './Header.scss'

export default function Header() {
 return (
  <div className='Header'>
   <Navbar />
  </div>
 )
}
