import Card from '../../Card/Card'

export default function Main() {
 return (
  <div className='Main'>
   <div className='Flex'>
    <div className='w-80'>
     <Card />
    </div>
    <div className='w-20'>
     <Card />
    </div>
   </div>
   <Card />
  </div>
 )
}
