import doc from '../assets/icons/doc.png'
import history from '../assets/icons/history.png'
import home from '../assets/icons/home.png'
import location from '../assets/icons/location.png'
import slot from '../assets/icons/slot.png'
import wallet from '../assets/icons/wallet.png'
export const menu = [
 {
  id: 1,
  icon: home,
  name: 'Accueil'
 },
 {
  id: 2,
  icon: wallet,
  name: 'Consultation Vidéo'
 },
 {
  id: 3,
  icon: history,
  name: 'Historique'
 },
 {
  id: 4,
  icon: slot,
  name: 'Mes rendez-vous'
 },
 {
  id: 5,
  icon: doc,
  name: 'Mes documents'
 },
 {
  id: 6,
  icon: location,
  name: 'Etats des Lieux'
 }
]
